//
//  ViewController.swift
//  Bablo Xchange
//
//  Created by Mikhailo on 12/1/17.
//  Copyright © 2017 Mikhailo. All rights reserved.
//

import UIKit
import Alamofire
import RxSwift
import RxAlamofire

class DetailViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var coinLabel: UILabel!
    @IBOutlet weak var changeDirectionOutl: UIButton!
    let disposeBag = DisposeBag()
    var currency = CryptoDataCell()
    
    @IBOutlet weak var coinTextField: UITextField!
    @IBOutlet weak var usdTextField: UITextField!
    @IBOutlet weak var convertBtn: UIButton!
    
    // MARK: - App Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(false, animated: true)
        coinLabel.text = currency.name
        changeDirectionOutl.transform = changeDirectionOutl.transform.rotated(by: CGFloat(Double.pi))
    }
    
    // MARK: - UI Actions
    @IBAction func changeDirection(_ sender: Any) {
        changeDirectionOutl.transform = changeDirectionOutl.transform.rotated(by: CGFloat(Double.pi))
        coinTextField.isEnabled = coinTextField.isEnabled ? false : true
        usdTextField.isEnabled = usdTextField.isEnabled ? false : true
    }
    
    @IBAction func convertPressed(_ sender: UIButton) {
        coinTextField.resignFirstResponder()
        usdTextField.resignFirstResponder()
        
        if coinTextField.isEnabled {
            let value = NumberFormatter().number(from: coinTextField.text ?? "")?.floatValue
            usdTextField.text = String(format: "%.2f", (value! * currency.priceUSD))
        } else {
            let value = NumberFormatter().number(from: usdTextField.text ?? "")?.floatValue
            coinTextField.text = String(format: "%.2f", (value! / currency.priceUSD))
        }
    }
    
}

