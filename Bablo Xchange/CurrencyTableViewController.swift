//
//  CurrencyTableViewController.swift
//  Bablo Xchange
//
//  Created by Mikhailo on 12/1/17.
//  Copyright © 2017 Mikhailo. All rights reserved.
//

import UIKit
import RxSwift
import RxAlamofire

class CurrencyTableViewController: UITableViewController {
    
    let disposeBag = DisposeBag()
    var cryptoArray = [CryptoDataCell]()
    
    // MARK: - App Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        
        loadCurrency()
        tableView.tableHeaderView  = ParallaxView.init(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 200))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    // MARK: - Table view functions

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cryptoArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        
        // Configure the cell...
        cell.textLabel?.text = cryptoArray[indexPath.row].name
        let numberedString = cryptoArray[indexPath.row].priceUSD
        cell.detailTextLabel?.text = String("$ \(numberedString)")

        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let translationTransform = CATransform3DTranslate(CATransform3DIdentity, -500, 400, 0)
        cell.layer.transform = translationTransform
        
        UIView.animate(withDuration: 0.15, delay: 0.1, options: .curveEaseInOut, animations: {
            cell.layer.transform = CATransform3DIdentity
        })
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let coin = cryptoArray[indexPath.row]
        
        performSegue(withIdentifier: "DetailVCSegue", sender: coin)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let headerView = self.tableView.tableHeaderView as! ParallaxView
        headerView.scrollViewDidScroll(scrollView: scrollView)
    }
    
    // MARK: - Other function
    
    func loadCurrency() {
        let urlString = "https://api.coinmarketcap.com/v1/ticker/"
        
        let coinObservable = json(.get, urlString)
          coinObservable.asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { coinJSON in
                
                if let coinArray = coinJSON as? Array<[String: AnyObject]> {
                    for coin in coinArray {
                        let priceUSDString = coin["price_usd"] as! String
                        let priceUSD = Float(priceUSDString)
                        let name = coin["name"] as! String
                        
                        let bablo = CryptoDataCell.init(name: name, priceUSD: priceUSD!)
                        self.cryptoArray.append(bablo)
                    }
                }
                
                self.tableView.reloadData()
            }, onError:{ e in
                self.displayError(e as NSError)
            }).disposed(by: disposeBag)
    }
    
    func displayError(_ error: NSError?) {
        if let e = error {
            let alertController = UIAlertController(title: "Error", message: e.localizedDescription, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                // do nothing...
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let coin = sender as? CryptoDataCell {
            if let detailViewController = segue.destination as? DetailViewController {
                detailViewController.currency = coin
            }
        }
    }
}
