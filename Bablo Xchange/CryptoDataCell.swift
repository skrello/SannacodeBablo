//
//  CryptoDataCell.swift
//  Bablo Xchange
//
//  Created by Mikhailo on 12/2/17.
//  Copyright © 2017 Mikhailo. All rights reserved.
//

struct CryptoDataCell {
    let name: String
    let priceUSD: Float
    
    init() {
        name = ""
        priceUSD = 0.0
    }
    
    init(name: String, priceUSD: Float) {
        self.name = name
        self.priceUSD = priceUSD
    }
}
